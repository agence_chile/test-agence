# Agence's Developer test.

Te pedimos que leas  las instrucciones a continuación con calma y las sigas al pie de la letra para evitar malos entendidos o inconvenientes, si tienes cualquier duda, comunícala a través de google hangouts a la dirección **gonzalo.bahamondez@agence.cl.**


## Instrucciones.

* El test tiene una complejidad estimada de **1 hora y media** para ser completado, a partir de este minuto, si demoras más no quiere decir que no debas enviarlo, sin embargo el retraso generará un descuento en la evaluación final.

* Crea un fork de este repositorio.

* Clónalo en tu maquina local o en tu entorno de desarrollo.

* Lee con precaución cada pregunta (las que vienen a continuación de estas instrucciones en este mismo archivo) y respóndela en la carpeta **respuestas** que está en el directorio raiz, por cada respuesta crea un archivo que tenga como nombre el número de la pregunta **EJ: 1.js o 1.txt** dependiendo del caso, en el interior del directorio respuestas.

* Finalmente sube tu proyecto a un repositorio en bitbucket o github y comparte la URL con nosotros.


## Preguntas

1.-
Escriba un programa, que a partir de un arreglo de números entregue su número mas grande. **EJ: 1, 20, 3  -> El mayor número es 20.**


2.- Asumiendo  que usted  posee un archivo de texto llamado input.txt  con el contenido “abc” en su interior en una sola línea y el siguiente código fuente.

```js
'use strict';

var fs = require('fs');

fs.readFile('input.txt', function (err, data) {
  if (err) {
    return console.log(err);
  }
  console.log('read1: ' + data.toString());
});

var data = fs.readFileSync('input.txt');

console.log('read2: ' + data.toString());
console.log('end!');
```

Indique el output que entregará el programa y explique detalladamente la ejecución del programa (call stack, callback queue, event loop).


3.-  Escriba un programa que imprima desde el 1 hasta el 100  sin usar sentencias de iteración como EJ: for, while, foreach, do while,  etc.


4.- Explique  que es un índice de en una tabla de una base de datos relacional SQL, sus beneficios y sus desventajas.



5.- Escriba un programa que dada una matriz cuadrada de tamaño N x N calcule la diferencia absoluta entre la suma de sus diagonales.


** Ejemplo de input:**

3

11 2 4

4 5 6

10 8 -12

** Ejemplo de output **

15

**Explicación:**

La primera linea del input es  N el largo de la matriz, las siguientes líneas son los valores de cada índice de la matriz.

La primera diagonal es **11 + 5 - 12 = 4**

La segunda diagonal es ** 4 + 5 + 10 = 19**

Diferencia **|4 - 19| = 15**